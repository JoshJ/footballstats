footballstats
=============

Software for tracking (american) football statistics and calculating rankings

Depends on:
* Apache Commons CSV Parser
* Apache Commons Math
* Guava

To use: run Season.java. For now it just sets fbsteams and games as variables directly in the code (since I just run this out of eclipse and modify stuff all the time).

Windows users: to convert the preformatted.csv to output:  
> C:\strawberry\perl\bin\perl.exe .\tools\convert.pl .\preformatted.csv > .\data\cfb2017results.csv  
Then open it in Notepad++ and change the encoding to UTF-8, because Strawberry Perl makes it the wrong format.  
It'll be really obvious if you forgot, as the header will be all messed up in the parser error message.