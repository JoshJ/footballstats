package com.joshuajustice.footballstats;

import java.util.HashSet;
import java.util.Set;

/**
 * Basic representation of a team.
 * 
 * @author Joshua Justice
 */
public class Team {
	private String name;
	private String conference;
	private Set<Game> games;

	public Team(String name, String conference) {
		this.name = name;
		this.conference = conference;
		games = new HashSet<>();
	}

	public void addGame(Game game) {
		games.add(game);
	}

	public Set<Game> getGames() {
		return games;
	}

	public String getConference() {
		return conference;
	}

	@Override
	public String toString() {
		return name;
	}
}
