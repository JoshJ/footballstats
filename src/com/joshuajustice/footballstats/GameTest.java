package com.joshuajustice.footballstats;

import static org.junit.Assert.*;

import org.junit.Test;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

/**
 * Basic unit tests for the Game class.
 * 
 * @author Joshua Justice
 */
public class GameTest {

	/**
	 * Test for the constructor and basic getters
	 * 
	 * <p>
	 * Test data based on 2014 Clean Old Fashioned Hate.
	 */
	@Test
	public void testGame() {
		Team tech = new Team("Georgia Tech", "ACC");
		Team dwags = new Team("Georgia", "SEC");
		Game cofh = new Game(tech, 30, dwags, 24);
		assertEquals(dwags, cofh.getLoser());
		assertEquals(tech, cofh.getWinner());
		assertEquals(6, cofh.getMargin());
		assertEquals(tech, cofh.getAwayTeam());
		assertEquals(dwags, cofh.getHomeTeam());
	}
}
