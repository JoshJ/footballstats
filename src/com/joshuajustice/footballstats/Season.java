package com.joshuajustice.footballstats;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.joshuajustice.footballstats.calculator.AdjustedWinPercentage;
//import com.joshuajustice.footballstats.calculator.Awards;
import com.joshuajustice.footballstats.calculator.BasicSoS;
import com.joshuajustice.footballstats.calculator.Calculator;
import com.joshuajustice.footballstats.calculator.Colley;
import com.joshuajustice.footballstats.calculator.Elo;
import com.joshuajustice.footballstats.calculator.MoVSoS;
import com.joshuajustice.footballstats.calculator.PercentileBasedSoS;
import com.joshuajustice.footballstats.calculator.TieredRanking;

/**
 * Parse a season from data files.
 * 
 * @author Joshua Justice
 */
public class Season {

	private Map<String, Team> teamMap;
	private Multimap<String, Team> conferences;

	public Season(File teamsFile, File gamesFile) throws IOException {
		teamMap = new HashMap<>();
		conferences = HashMultimap.create();
		System.out.println("Creating teams parser...");
		parseTeams(teamsFile);
		System.out.println("Creating games parser...");
		parseGames(gamesFile);

		// Now we're done reading in the season and can just hand the teamMap to
		// some calculators.
		List<Calculator> rankings = new ArrayList<>();
		Set<Calculator> forPercentiles = new HashSet<>();
		
		AdjustedWinPercentage awp = new AdjustedWinPercentage(teamMap.values());
		Calculator basicSoS = new BasicSoS(teamMap.values(), awp);
		Calculator movSoS = new MoVSoS(teamMap.values(), awp, 7, 21);
		//Calculator awards = new Awards(teamMap.values(), awp);
		Calculator elo = new Elo(teamMap.values(), false);
		Calculator eloScore = new Elo(teamMap.values(), true);
		Calculator colley = new Colley(teamMap.values());
		Calculator tieredRank = new TieredRanking(teamMap.values());

		forPercentiles.add(basicSoS);
		forPercentiles.add(movSoS);
		//forPercentiles.add(awards);
		forPercentiles.add(elo);
		forPercentiles.add(eloScore);
		forPercentiles.add(colley);
		forPercentiles.add(tieredRank);

		Calculator percentile = new PercentileBasedSoS(teamMap.values(), forPercentiles, false);

		rankings.add(basicSoS);
		rankings.add(movSoS);
		//rankings.add(awards);
		rankings.add(elo);
		rankings.add(eloScore);
		rankings.add(colley);
		rankings.add(tieredRank);
		rankings.add(percentile);

		TotalRanking total = new TotalRanking(rankings);
		VoteRanking voted = new VoteRanking(rankings, total);
		System.out.println(voted.toMarkdown());
	}

	void parseTeams(File teamsFile) throws IOException {
		CSVParser parser = CSVParser.parse(teamsFile, Charset.defaultCharset(), CSVFormat.DEFAULT.withHeader());
		// Create a catch-all entry for FCS teams.
		Team fcs = new Team("FCS", "FCS");
		teamMap.put("FCS", fcs);
		for (CSVRecord record : parser.getRecords()) {
			String teamName = record.get("Team");
			String conference = record.get("Conference");
			if (teamMap.get(teamName) == null) {
				Team team = new Team(teamName, conference);
				teamMap.put(teamName, team);
				conferences.put(conference, team);
			}
		}
		System.out.println("Identified: " + teamMap.keySet().size() + " teams");
	}

	void parseGames(File gamesFile) throws IOException {
		CSVParser parser = CSVParser.parse(gamesFile, Charset.defaultCharset(), CSVFormat.DEFAULT.withHeader());
		System.out.println("Parser created. Parsing...");
		System.out.println("headers: " + parser.getHeaderMap());
		for (CSVRecord record : parser.getRecords()) {
			String awayName = record.get("Visitor");
			String homeName = record.get("Home Team");
			int awayScore = -1;
			int homeScore = -1;
			try {
				awayScore = Integer.parseInt(record.get("Visitor Score"));
				homeScore = Integer.parseInt(record.get("Home Score"));
			} catch (NumberFormatException ex) {
				System.out.println("BAD ROW:" + record);
				// The record is bad, skip it
				// This happens if a game is delayed and there's no record, for instance
				// or if you have records for games that haven't been played yet.
				continue;
			}
			// Convert the string representations of the teams into Team objects
			Team away = null;
			Team home = null;
			if (teamMap.get(awayName) != null) {
				away = teamMap.get(awayName);
			} else {
				away = teamMap.get("FCS");
			}
			if (teamMap.get(homeName) != null) {
				home = teamMap.get(homeName);
			} else {
				home = teamMap.get("FCS");
			}
			// Sanity check the data
			if (away == null) {
				System.out.println("ERROR: away team name " + awayName + "was not in team map");
			}
			// Now let's create the games.
			// We don't need to keep a reference to them here, the Teams do so
			new Game(away, awayScore, home, homeScore);
		}
		System.out.println("Parsing complete.");

	}

	public static void main(String[] args) throws IOException {
		String fbsteams = "data/fbsteams.csv";
		String games = "data/cfb2017results.csv";
		File teamsFile = new File(fbsteams);
		File gamesFile = new File(games);
		new Season(teamsFile, gamesFile);
	}
}
