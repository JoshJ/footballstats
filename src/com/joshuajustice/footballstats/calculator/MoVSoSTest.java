package com.joshuajustice.footballstats.calculator;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

public class MoVSoSTest {

	@Test
	public void testMoVSoSBasic() {
		Team twozero = new Team("TwoZero", "X");
		Team oneone = new Team("OneOne", "X");
		Team zerotwo = new Team("ZeroTwo", "X");

		new Game(twozero, 7, zerotwo, 0);
		new Game(oneone, 7, zerotwo, 0);
		new Game(twozero, 7, oneone, 0);

		Set<Team> teams = new HashSet<>();
		teams.add(twozero);
		teams.add(oneone);
		teams.add(zerotwo);

		AdjustedWinPercentage awp = new AdjustedWinPercentage(teams);
		MoVSoS movSos = new MoVSoS(teams, awp, 0, 999999); // no bonus for winning, absurdly high cap

		double epsilon = 0.000001;
		assertEquals(2.625, movSos.getRawValueForTeam(twozero), epsilon);
		assertEquals(0.0, movSos.getRawValueForTeam(oneone), epsilon);
		assertEquals(-2.625, movSos.getRawValueForTeam(zerotwo), epsilon);
		
	}
	
	@Test
	public void testMoVSoSBonusAndCap() {
		Team twozero = new Team("TwoZero", "X");
		Team oneone = new Team("OneOne", "X");
		Team zerotwo = new Team("ZeroTwo", "X");

		new Game(twozero, 99999, zerotwo, 0);
		new Game(oneone, 7, zerotwo, 0);
		new Game(twozero, 7, oneone, 0);

		Set<Team> teams = new HashSet<>();
		teams.add(twozero);
		teams.add(oneone);
		teams.add(zerotwo);

		AdjustedWinPercentage awp = new AdjustedWinPercentage(teams);
		MoVSoS movSos = new MoVSoS(teams, awp, 7, 21);

		double epsilon = 0.000001;
		assertEquals(7.0, movSos.getRawValueForTeam(twozero), epsilon);
		assertEquals(0.0, movSos.getRawValueForTeam(oneone), epsilon);
		assertEquals(-7.0, movSos.getRawValueForTeam(zerotwo), epsilon);
		
	}

}
