package com.joshuajustice.footballstats.calculator;

import java.util.Collection;
import java.util.HashMap;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

public class Awards extends Calculator {

	public Awards(Collection<Team> teams, AdjustedWinPercentage awp) {
		rawMap = new HashMap<>();
		for (Team team : teams) {
			double awards = getAwardsForTeam(team, awp);
			rawMap.put(team, awards);
		}
	}
	
	private double getAwardsForTeam(Team team, AdjustedWinPercentage awp) {
		int totalGames = team.getGames().size();
		double rawValue = 0.0;
		for (Game game : team.getGames()) {
			double gameValue = 0;
			if (team == game.getWinner()) {
				gameValue += 1; // A win is always worth 1 point to start with.
				if (team == game.getAwayTeam()) {
					gameValue += 0.5; // Winning away is worth half a point.
				}
				double difficulty = awp.getRawValueForTeam(game.getLoser());
				gameValue += difficulty; // A game is worth more the harder the opponent is to beat.
				double marginBonus = 0;
				if (game.getMargin() >= 14) {
					marginBonus = 0.5; // A 2-TD lead is worth another half a point.
				}
				gameValue += marginBonus; // A 2-TD lead is worth another half a point.
			} else {
				gameValue -= 1; // A loss is always worth -1 point to start with.
				if (team == game.getHomeTeam()) {
					gameValue -= 0.5; // Get run out of your own home stadium? Lose half a point.
				}
				double difficulty = awp.getRawValueForTeam(game.getWinner());
				gameValue -= (1 - difficulty); // The worse the team you lost to, the more you lose.
				double marginPenalty = 0;
				if (game.getMargin() >= 14) {
					marginPenalty = -0.5; // Losing by 2 TDs is worth losing another half a point.
				}
				gameValue += marginPenalty;
			}
			rawValue += gameValue;
		}
		// integer division will almost always be zero, let's not have that
		if (totalGames == 0) {
			// A team with no games should be at zero, but NaN sorts above
			// positive values.
			return 0;
		}
		double awardsPerGame = rawValue / (double) totalGames;
		return awardsPerGame;
	}

	@Override
	public String getFormulaName() {
		return "Awards";
	}
}
