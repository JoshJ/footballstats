package com.joshuajustice.footballstats.calculator;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

public class BasicSoSTest {

	@Test
	public void testBasicSoS() {
		Team twozero = new Team("TwoZero", "X");
		Team oneone = new Team("OneOne", "X");
		Team zerotwo = new Team("ZeroTwo", "X");

		new Game(twozero, 7, zerotwo, 0);
		new Game(oneone, 7, zerotwo, 0);
		new Game(twozero, 7, oneone, 0);

		Set<Team> teams = new HashSet<>();
		teams.add(twozero);
		teams.add(oneone);
		teams.add(zerotwo);

		AdjustedWinPercentage awp = new AdjustedWinPercentage(teams);
		BasicSoS sos = new BasicSoS(teams, awp);

		double epsilon = 0.000001;
		// 3/8 makes sense. That's the average of (1/4) and (1/2).
		assertEquals(0.375, sos.getRawValueForTeam(twozero), epsilon);
		assertEquals(0.0, sos.getRawValueForTeam(oneone), epsilon);
		assertEquals(-0.375, sos.getRawValueForTeam(zerotwo), epsilon);
		
	}

}
