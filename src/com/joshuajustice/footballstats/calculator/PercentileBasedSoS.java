package com.joshuajustice.footballstats.calculator;

import java.util.Collection;
import java.util.HashMap;

import com.google.common.annotations.VisibleForTesting;
import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

public class PercentileBasedSoS extends Calculator {

	private boolean includeScore;

	public PercentileBasedSoS(Collection<Team> teams, Collection<Calculator> calculators, boolean includeScore) {
		rawMap = new HashMap<>();
		this.includeScore = includeScore;
		for (Team team : teams) {
			double pctRank = getPctRankForTeam(team, calculators, includeScore);
			rawMap.put(team, pctRank);
		}
	}

	@VisibleForTesting
	double getPctRankForTeam(Team team, Collection<Calculator> calculators, boolean includeScore) {
		int totalGames = team.getGames().size();
		double rawValue = 0.0;
		for (Game game : team.getGames()) {
			double gameValue = 0.0;
			Team otherTeam = null;
			if (game.getWinner() == team) {
				otherTeam = game.getLoser();
				double strength = getPercentileForTeam(otherTeam, calculators);
				if (includeScore) {
					gameValue += strength * game.getMargin();
				} else {
					gameValue += strength;
				}
			} else {
				otherTeam = game.getWinner();
				double strength = 1 - getPercentileForTeam(otherTeam, calculators);
				if (includeScore) {
					gameValue -= strength * game.getMargin();
				} else {
					gameValue -= strength;
				}
			}
			rawValue += gameValue;
		}
		// integer division will almost always be zero, let's not have that
		double perGame = rawValue / (double) totalGames;
		if (totalGames == 0) {
			// A team with no games should be at zero, but NaN sorts above
			// positive values.
			return 0;
		}
		return perGame;
	}

	/**
	 * Not to be confused with the one from the parent class.
	 * 
	 * @param team
	 * @param calculators
	 * @return
	 */
	double getPercentileForTeam(Team team, Collection<Calculator> calculators) {
		double value = 0.0;
		for (Calculator calculator : calculators) {
			value += calculator.getPercentileForTeam(team);
		}
		return value / (double) calculators.size();
	}

	public String getFormulaName() {
		if (includeScore) {
			return "PctMoV";
		} else {
			return "Pct";
		}
	}

}
