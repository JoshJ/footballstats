package com.joshuajustice.footballstats.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.joshuajustice.footballstats.Team;

/**
 * The final step after calculating rankings is returning a list of teams.
 * 
 * <p>
 * This is a class to order a map of teams by their ranking.
 * 
 * @author joshj777
 */
public class TeamMapSorter {

	/**
	 * Sort a Team - metric map by its values, descending.
	 * 
	 * <p>
	 * Most ratings are higher-number-better, so you probably want this one.
	 * 
	 * @param map
	 *            of Teams to a metric scored as Doubles
	 * @return a sorted list of Teams
	 */
	public static List<Team> sortTeamMapDescending(Map<Team, Double> map) {
		List<Entry<Team, Double>> list = new ArrayList<>(map.entrySet());
		Collections.sort(list, new Comparator<Entry<Team, Double>>() {
			public int compare(Entry<Team, Double> one, Entry<Team, Double> two) {
				return two.getValue().compareTo(one.getValue());
			}
		});
		List<Team> result = new ArrayList<>();
		for (Entry<Team, Double> entry : list) {
			result.add(entry.getKey());
		}
		return result;
	}

	/**
	 * Sort a Team - metric map by its values, ascending
	 * 
	 * @param map
	 *            of Teams to a metric scored as Doubles
	 * @return a sorted list of Teams
	 */
	public static List<Team> sortTeamMapAscending(Map<Team, Double> map) {
		List<Entry<Team, Double>> list = new ArrayList<>(map.entrySet());
		Collections.sort(list, new Comparator<Entry<Team, Double>>() {
			public int compare(Entry<Team, Double> one, Entry<Team, Double> two) {
				return one.getValue().compareTo(two.getValue());
			}
		});
		List<Team> result = new ArrayList<>();
		for (Entry<Team, Double> entry : list) {
			result.add(entry.getKey());
		}
		return result;
	}

	/**
	 * Sort a Team - metric map by its values, descending, and keep the values.
	 * 
	 * <p>
	 * This is only ever going to want to be done in an ascending form.
	 * 
	 * @param map
	 *            of Teams to a metric scored as Doubles
	 * @return a sorted list of Teams
	 */
	public static List<Entry<Team, Double>> sortMapWithValues(Map<Team, Double> map) {
		List<Entry<Team, Double>> list = new ArrayList<>(map.entrySet());
		Collections.sort(list, new Comparator<Entry<Team, Double>>() {
			public int compare(Entry<Team, Double> one, Entry<Team, Double> two) {
				return one.getValue().compareTo(two.getValue());
			}
		});
		return list;
	}

}
