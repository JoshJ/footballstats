package com.joshuajustice.footballstats.calculator;

import java.util.Collection;
import java.util.HashMap;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

/**
 * A calculator based on the premise that a win should be "worth" a score equal
 * to the opponent's win rate.
 * 
 * This is the most primitive ranking system that produces workable results.
 * 
 * @author Joshua Justice
 *
 */
public class BasicSoS extends Calculator {

	public BasicSoS(Collection<Team> teams, AdjustedWinPercentage awp) {
		rawMap = new HashMap<>();
		for (Team team : teams) {
			double sos = getSoSForTeam(team, awp);
			rawMap.put(team, sos);
		}
	}

	private double getSoSForTeam(Team team, AdjustedWinPercentage awp) {
		int totalGames = team.getGames().size();
		double rawValue = 0.0;
		for (Game game : team.getGames()) {
			if (game.getWinner() == team) {
				Team otherTeam = game.getLoser();
				// For a case where a team wins, its SoS gained is the win rate
				// of the other team
				rawValue += awp.getRawValueForTeam(otherTeam);
			} else {
				Team otherTeam = game.getWinner();
				// For a case where a team loses, its SoS lost is 1 minus the
				// win rate of the other team
				// (Consider: if you lose to a bad team (0.2 win rate) you
				// should lose 0.8)
				rawValue -= (1 - awp.getRawValueForTeam(otherTeam));

			}
		}
		// integer division will almost always be zero, let's not have that
		double sosPerGame = rawValue / (double) totalGames;
		if (totalGames == 0) {
			// A team with no games should be at zero, but NaN sorts above
			// positive values.
			return 0;
		}
		return sosPerGame;
	}

	@Override
	public String getFormulaName() {
		return "BasicSoS";
	}

}
