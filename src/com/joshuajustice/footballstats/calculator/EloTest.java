package com.joshuajustice.footballstats.calculator;

import static org.junit.Assert.*;

import org.junit.Test;

public class EloTest {

	/**
	 * Rather than trying to test the Elo output for entire teams (which would merely be an exercise in
	 * copy-pasting the final value into the test, proving nothing), this tests at the level of the Elo math. 
	 */
	@Test
	public void testElo() {

		double epsilon = 0.0001;
		assertEquals(24.0, Elo.calculateChange(1200, 1200, 48, 1), epsilon);
		assertEquals(11.5321, Elo.calculateChange(1400, 1200, 48, 1), epsilon);
		assertEquals(24.0 + (24.0 - 11.5321), Elo.calculateChange(1200, 1400, 48, 1), epsilon);
		
	}

}
