package com.joshuajustice.footballstats.calculator;

import java.util.List;
import java.util.Map;

import com.joshuajustice.footballstats.Team;

public abstract class Calculator {
	/**
	 * The map with raw values for each team, where those values are whatever
	 * this calculator ranks on.
	 */
	protected Map<Team, Double> rawMap;

	/**
	 * Get the rankings of teams by this formula.
	 */
	public List<Team> getRanking() {
		return TeamMapSorter.sortTeamMapDescending(rawMap);
	}

	/**
	 * Get the percentile ranking of a team.
	 * <p>
	 * The percentile ranking of a team is how many of the teams its rating is
	 * better than. As such, a high score is better.
	 * 
	 * @param team
	 *            whose percentile to look up
	 * @return The percentile ranking in the range [0.0, 1.0)
	 */
	public double getPercentileForTeam(Team team) {
		double winrate = rawMap.get(team);
		double numerator = 0.0;
		double denominator = rawMap.values().size();
		for (Double otherWinrate : rawMap.values()) {
			if (winrate > otherWinrate) {
				numerator += 1.0;
			}
		}
		return numerator / denominator;
	}

	/**
	 * Get the human-readable name of the calculator's formula for printout
	 * purposes. Must override in subclass.
	 */
	public abstract String getFormulaName();

	/**
	 * Some formulas are used as parts of other formulas. This provides a ready
	 * lookup mechanism.
	 * 
	 * @param team
	 * @return The adjusted win percentage for that team.
	 */
	public Double getRawValueForTeam(Team team) {
		return rawMap.get(team);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("========================================================================\n");
		builder.append(getFormulaName() + "\n");
		int position = 0;
		for (Team team : getRanking()) {
			position++;
			double raw = getRawValueForTeam(team);
			builder.append(position + "," + team + "," + raw + "\n");
		}
		builder.append("========================================================================\n");
		return builder.toString();
	}
}
