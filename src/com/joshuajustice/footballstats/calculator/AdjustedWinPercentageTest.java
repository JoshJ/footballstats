package com.joshuajustice.footballstats.calculator;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;
import com.joshuajustice.footballstats.calculator.AdjustedWinPercentage;

public class AdjustedWinPercentageTest {

	@Test
	public void testAWP() {
		Team twozero = new Team("TwoZero", "X");
		Team oneone = new Team("OneOne", "X");
		Team zerotwo = new Team("ZeroTwo", "X");

		new Game(twozero, 7, zerotwo, 0);
		new Game(oneone, 7, zerotwo, 0);
		new Game(twozero, 7, oneone, 3);

		Set<Team> teams = new HashSet<>();
		teams.add(twozero);
		teams.add(oneone);
		teams.add(zerotwo);

		AdjustedWinPercentage calculator = new AdjustedWinPercentage(teams);
		double epsilon = 0.000001;
		assertEquals(0.75, calculator.getWinRateForTeam(twozero), epsilon);
		assertEquals(0.5, calculator.getWinRateForTeam(oneone), epsilon);
		assertEquals(0.25, calculator.getWinRateForTeam(zerotwo), epsilon);
		assertEquals(0.66666666, calculator.getPercentileForTeam(twozero), epsilon);
		assertEquals(0.33333333, calculator.getPercentileForTeam(oneone), epsilon);
		assertEquals(0.0, calculator.getPercentileForTeam(zerotwo), epsilon);

	}

}
