package com.joshuajustice.footballstats.calculator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.google.common.annotations.VisibleForTesting;
import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

/**
 * A modified Elo-style calculator which ensures that the order in which games
 * are played does not matter. Optionally accepts a parameter to include score
 * in the calculation.
 * 
 * <p>
 * If score is included in the calculation, the game is worth (points / total
 * points), otherwise a win is worth 1 point and a loss is worth 0.
 * 
 * @author Joshua Justice
 */
public class Elo extends Calculator {
	private boolean includeScore;

	public Elo(Collection<Team> teams, boolean includeScore) {
		this.includeScore = includeScore;
		rawMap = new HashMap<>();
		for (Team team : teams) {
			rawMap.put(team, 1600.0); // Starting Elo value
		}
		// Shrink K every generation so scores are sure to converge.
		for (double k = 128.0; k > 0; k--) {
			calculateElo(k, includeScore);
		}
		double total = 0;
		for (Team team: teams) {
			total += rawMap.get(team);
		}
		double average = total / (double) teams.size();
		if (includeScore) {
			System.out.println("Average EloScore rating is " + average);
		} else {
			System.out.println("Average Elo rating is " + average);
		}
	}

	@VisibleForTesting
	void calculateElo(double k, boolean includeScore) {
		Map<Team, Double> nextGeneration = new HashMap<>();
		for (Team team : rawMap.keySet()) {
			double elo = rawMap.get(team);
			double change = 0.0;
			for (Game game : team.getGames()) {
				Team otherTeam = null;
				double gameScore = 0.5;
				if (team == game.getWinner()) {
					if (!includeScore) {
						gameScore = 1;
					}
					otherTeam = game.getLoser();
				} else {
					if (!includeScore) {
						gameScore = 0;
					}
					otherTeam = game.getWinner();
				}
				if (includeScore) {
					double teamPoints = (double) game.getScore(team);
					double totalPoints = teamPoints + (double) game.getScore(otherTeam);
					gameScore = teamPoints / totalPoints;
					//gameScore = modifyGameScore(gameScore);
				}
				change += calculateChange(elo, rawMap.get(otherTeam), k, gameScore);
			}
			nextGeneration.put(team, elo + change);
		}

		rawMap = nextGeneration;
	}

	double modifyGameScore(double gameScore) {
		// This particular version of modifyGameScore is no good, as it breaks the Elo formula's guarantee
		// of keeping scores constant.
		if (gameScore < 0.5) {
			gameScore = Math.pow(gameScore, 2.0);
		} else if (gameScore > 0.5) {
			gameScore = Math.sqrt(gameScore);
		}
		return gameScore;
	}

	/**
	 * 
	 * @param first
	 *            The Elo of the first team from the previous generation
	 * @param second
	 *            The Elo of the second team from the previous generation
	 * @param k
	 *            The Elo K-Value
	 * @param gameScore
	 *            Either the percentage of points scored, or 0 for loss/1 for
	 *            win.
	 * @return
	 */
	@VisibleForTesting
	static double calculateChange(double first, double second, double k, double gameScore) {
		double expectedScore = 1.0 / (1.0 + Math.pow(10.0, (second - first) / 400));
		double change = k * (gameScore - expectedScore);
		return change;
	}

	public String getFormulaName() {
		if (!includeScore) {
			return "Elo";
		} else {
			return "EloScore";
		}
	}

}
