package com.joshuajustice.footballstats.calculator;

import java.util.Collection;
import java.util.HashMap;

import com.google.common.annotations.VisibleForTesting;
import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

/**
 * Adjusted win percentage is (wins+1) / (wins+losses+2).
 * 
 * <p>
 * The +1/+2 is intended to prevent multiply by zero from "collapsing" ratings
 * to zero regardless of how many games were played.
 * <p>
 * This is not a truly viable ranking system and should not take part in final
 * rankings.
 * <p>
 * However, it is enormously useful as input to a great many other ranking
 * systems.
 * 
 * @author joshj777
 */
public class AdjustedWinPercentage extends Calculator {

	public AdjustedWinPercentage(Collection<Team> teams) {
		rawMap = new HashMap<>();
		for (Team team : teams) {
			double winrate = getWinRateForTeam(team);
			rawMap.put(team, winrate);
		}
	}

	@VisibleForTesting
	double getWinRateForTeam(Team team) {
		int totalGames = team.getGames().size() + 2;
		int wins = 1;
		for (Game game : team.getGames()) {
			if (game.getWinner() == team) {
				wins++;
			}
		}
		// integer division will almost always be zero, let's not have that
		double winrate = (double) wins / (double) totalGames;
		return winrate;
	}

	@Override
	public String getFormulaName() {
		return "Adjusted Win Percentage";
	}

}
