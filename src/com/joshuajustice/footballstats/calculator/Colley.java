package com.joshuajustice.footballstats.calculator;

import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import com.google.common.annotations.VisibleForTesting;
import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

/**
 * Implementation of the Colley Matrix in Java.
 * 
 * <p>
 * The Colley Matrix algorithm is really a combination of three matrices: Cr=b
 * 
 * <p>
 * First, the matrix "C" (the Colley Matrix proper), which is simply defined as
 * a row for every team, with a column for every possible, with -1 entered for
 * each game played against an opponent (and 0 for opponents not played). -2, -3
 * etc for multiple games aginst the same opponent. In the entry for the team
 * itself (in other words, C[i,i] ) is the number of games that team had, plus
 * two.
 * 
 * <p>
 * Second, the column matrix "r" of rankings to be solved for, one for each
 * team.
 * 
 * <p>
 * On the right side is the column matrix, "b", for which each row's entries
 * are: b[i] = 1 + (wins − losses) / 2.
 * 
 * <p>
 * The entries for each team in "r" is the ranking for the teams, which begins
 * unsolved.
 * 
 * <p>
 * Note that the downside to the Colley method is that it doesn't really take
 * into account which team any given win or loss was to - it's just based on the
 * record vs the schedule. This is a valid criticism, but the formula itself is
 * quite robust, so it's worth including.
 * 
 * <p>
 * Unlike Colley's original implementation, this implementation, as with all
 * other code in this project, includes games vs FCS teams. At the moment, FCS
 * teams are regarded as one singular opponent regardless of whether it's
 * Savannah State or North Dakota State, which is less than ideal, but I'm
 * somewhat constrained by my dataset at the moment. Once the dataset is
 * expanded to include all games, I'll naturally have to revisit this.
 * 
 * <p>
 * I make no claim to the Colley Matrix algorithm, as described by Wesley Colley
 * in his paper or website. This is merely an implementation. You will note that
 * Colley's description of Laplace's Method is exactly what I use elsewhere for
 * calculating the "adjusted win percentage". I referred to it as AWP because
 * I've played various forms of Counter-Strike, and am easily amused. It could
 * just as easily be called the Laplace formula.
 * 
 * @author Joshua Justice
 *
 */
public class Colley extends Calculator {

	private HashMap<Team, Integer> teamNumber;

	RealMatrix C;
	RealVector b;

	public Colley(Collection<Team> teams) {
		rawMap = new HashMap<>();
		// The C matrix is initialized to all zeroes.
		C = new Array2DRowRealMatrix(teams.size(), teams.size());
		for (int i = 0; i < teams.size(); i++) {
			for (int j = 0; j < teams.size(); j++) {
				C.setEntry(i, j, 0.0);
			}
		}
		b = new ArrayRealVector(teams.size());

		// Make a simple map of team -> number so it's trivial to have a lookup.
		// Could use a list and find() for small sizes, but this will be better
		// if I ever
		// get all the teams in from FBS to NAIA.
		teamNumber = new HashMap<>();
		int i = 0;
		for (Team team : teams) {
			teamNumber.put(team, i);
			i++;
		}
		// Have to get all the numbers before we can fill the matrix.
		for (Team team : teams) {
			fillMatrixForTeam(team);
		}

		RealVector r = solveTheSystem(C, b);
		// Once again, we have to go through all the teams, only this time it's
		// to get them in rawMap where they belong.
		double total = 0.0;
		for (Team team : teams) {
			double val = r.getEntry(teamNumber.get(team));
			total += val;
			rawMap.put(team, val);
		}
		double average = total / (double) teams.size();
		System.out.println("Average Colley ranking: " + average);
	}

	private void fillMatrixForTeam(Team team) {
		int teamNum = teamNumber.get(team);
		int wins = 0;
		int totalGames = team.getGames().size();
		int otherTeamNum = -1; // Sentinel value, will produce an error
		for (Game game : team.getGames()) {
			if (team == game.getWinner()) {
				otherTeamNum = teamNumber.get(game.getLoser());
				wins++;
			} else {
				otherTeamNum = teamNumber.get(game.getWinner());
			}
			C.addToEntry(teamNum, otherTeamNum, -1); // addToEntry in case of
														// multiple games.
		}
		C.setEntry(teamNum, teamNum, 2 + totalGames);
		int losses = totalGames - wins;
		double bVal = 1 + (wins - losses) / 2.0;
		b.setEntry(teamNum, bVal);
	}

	/**
	 * This solves the Cr = b algorithm.
	 */
	@VisibleForTesting
	RealVector solveTheSystem(RealMatrix C, RealVector b) {
		DecompositionSolver solver = new LUDecomposition(C).getSolver();
		RealVector solution = solver.solve(b);
		return solution;
	}

	@Override
	public String getFormulaName() {
		return "Colley";
	}

}
