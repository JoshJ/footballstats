package com.joshuajustice.footballstats.calculator;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.joshuajustice.footballstats.Team;
import com.joshuajustice.footballstats.calculator.TeamMapSorter;

public class TeamMapSorterTest {

	@Test
	public void testSortDescending() {
		Team undefeated = new Team("Undefeated", "X");
		Team loser = new Team("Loser", "X");
		Team oneone = new Team("OneOne", "X");
		Map<Team, Double> map = new HashMap<>();
		map.put(oneone, .5);
		map.put(undefeated, 1.0);
		map.put(loser, 0.0);

		List<Team> teams = TeamMapSorter.sortTeamMapDescending(map);
		assertEquals(undefeated, teams.get(0));
		assertEquals(oneone, teams.get(1));
		assertEquals(loser, teams.get(2));
	}

	@Test
	public void testSortAscending() {
		Team shutouts = new Team("Shutouts", "X");
		Team pushover = new Team("Pushover", "X");
		Team twotds = new Team("TwoTDs", "X");
		Map<Team, Double> map = new HashMap<>();
		map.put(twotds, 14.0);
		map.put(shutouts, 0.0);
		map.put(pushover, 222.0);

		List<Team> teams = TeamMapSorter.sortTeamMapAscending(map);
		assertEquals(shutouts, teams.get(0));
		assertEquals(twotds, teams.get(1));
		assertEquals(pushover, teams.get(2));
	}

}
