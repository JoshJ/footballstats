package com.joshuajustice.footballstats.calculator;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

public class AwardsTest {

	@Test
	public void testAwards() {
		Team twozero = new Team("TwoZero", "X");
		Team oneone = new Team("OneOne", "X");
		Team zerotwo = new Team("ZeroTwo", "X");

		new Game(twozero, 14, zerotwo, 0);
		new Game(oneone, 14, zerotwo, 7);
		new Game(twozero, 14, oneone, 7);

		Set<Team> teams = new HashSet<>();
		teams.add(twozero);
		teams.add(oneone);
		teams.add(zerotwo);

		AdjustedWinPercentage awp = new AdjustedWinPercentage(teams);
		Awards awards = new Awards(teams, awp);

		double epsilon = 0.000001;
		assertEquals(2.125, awards.getRawValueForTeam(twozero), epsilon);
		assertEquals(0.0, awards.getRawValueForTeam(oneone), epsilon);
		assertEquals(-2.125, awards.getRawValueForTeam(zerotwo), epsilon);
		
		
	}

}
