package com.joshuajustice.footballstats.calculator;

import java.util.Collection;
import java.util.HashMap;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

/**
 * A calculator based on the premise that a win should be "worth" a score equal
 * to the opponent's win rate, with margin of victory factored in.
 * 
 * This is the most primitive MoV based ranking system that produces workable
 * results.
 * 
 * @author Joshua Justice
 *
 */
public class MoVSoS extends Calculator {

	/**
	 * Construct a MoV-SoS calculator.
	 * 
	 * @param teams
	 *            The set of teams.
	 * @param awp
	 *            A calculator with AWP.
	 * @param winBonus
	 *            The amount of points winning should be worth.
	 * @param capMargin
	 *            The largest allowable margin of victory.
	 */
	public MoVSoS(Collection<Team> teams, AdjustedWinPercentage awp, int winBonus, int capMargin) {
		rawMap = new HashMap<>();
		for (Team team : teams) {
			double sos = getMoVSoSForTeam(team, awp, winBonus, capMargin);
			rawMap.put(team, sos);
		}
	}

	private double getMoVSoSForTeam(Team team, AdjustedWinPercentage awp, int winBonus, int capMargin) {
		int totalGames = team.getGames().size();
		double rawValue = 0.0;
		for (Game game : team.getGames()) {
			int margin = Math.min((game.getMargin() + winBonus), (capMargin + winBonus));
			double gameValue = 0.0;
			Team otherTeam = null;
			if (game.getWinner() == team) {
				otherTeam = game.getLoser();
				// For a case where a team wins, its SoS gained is the win rate
				// of the other team
				// This is multiplied by max((margin + winBonus), capMargin).
				gameValue = awp.getRawValueForTeam(otherTeam) * margin;
				rawValue += gameValue;
			} else {
				otherTeam = game.getWinner();
				// For a case where a team loses, its SoS lost is 1 minus the
				// win rate of the other team
				// (Consider: if you lose to a bad team (0.2 win rate) you
				// should lose 0.8 * margin)
				// This is multiplied by the margin of victory of the winning
				// team.
				// Uncapped because if you get stomped you should be penalized
				// for it.
				gameValue = (1 - awp.getRawValueForTeam(otherTeam)) * margin;
				rawValue -= gameValue;
			}
		}
		// integer division is bad, let's not have that
		double movSosPerGame = rawValue / (double) totalGames;
		if (totalGames == 0) {
			// A team with no games should be at zero, but NaN sorts above
			// positive values.
			return 0;
		}
		return movSosPerGame;
	}

	@Override
	public String getFormulaName() {
		return "MoVSoS";
	}

}
