package com.joshuajustice.footballstats.calculator;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

public class ColleyTest {

	@Test
	public void testColleyTwoTeams() {
		Team winner = new Team("Winner", "X");
		Team loser = new Team("ZeroTwo", "X");
		new Game(winner, 7, loser, 0);
		Set<Team> teams = new HashSet<>();
		teams.add(winner);
		teams.add(loser);
		
		Colley colley = new Colley(teams);

		double epsilon = 0.000001;
		// Colley's paper states (on page 9) that 3/8 is the correct value for the loser
		// (And therefore, 5/8 for the winner).
		assertEquals(0.625, colley.getRawValueForTeam(winner), epsilon);
		assertEquals(0.375, colley.getRawValueForTeam(loser), epsilon);
	}

	/**
	 * Colley's paper also describes a 5-team scenario. This should match that result.
	 */
	@Test
	public void testColleyFiveTeams() {
		Team a = new Team("a", "X");
		Team b = new Team("b", "X");
		Team c = new Team("b", "X");
		Team d = new Team("b", "X");
		Team e = new Team("b", "X");
		
		new Game(a, 7, c, 0);
		new Game(a, 0, d, 7);
		new Game(a, 0, e, 7);
		new Game(b, 0, c, 7);
		new Game(b, 7, e, 0);
		new Game(c, 7, d, 0);
		new Game(c, 0, e, 7);
		
		
		Set<Team> teams = new HashSet<>();
		teams.add(a);
		teams.add(b);
		teams.add(c);
		teams.add(d);
		teams.add(e);
		
		Colley colley = new Colley(teams);

		double epsilon = 0.001; // Reduced epsilon value to match his paper's sigfigs 
		assertEquals(0.413, colley.getRawValueForTeam(a), epsilon);
		assertEquals(0.522, colley.getRawValueForTeam(b), epsilon);
		assertEquals(0.500, colley.getRawValueForTeam(c), epsilon);
		assertEquals(0.478, colley.getRawValueForTeam(d), epsilon);
		assertEquals(0.587, colley.getRawValueForTeam(e), epsilon);
	}
}
