package com.joshuajustice.footballstats.calculator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.google.common.annotations.VisibleForTesting;
import com.joshuajustice.footballstats.Game;
import com.joshuajustice.footballstats.Team;

public class TieredRanking extends Calculator {

	private double[] CLEARWIN = { 1.0, 2.0, 3.0, 4.0, 5.0 };
	private double[] CLOSEWIN = { 0.4, 1.5, 2.7, 3.6, 5.0 };
	private double[] CLEARLOSS = { -5.0, -4.0, -3.0, -2.0, -1.0 };
	private double[] CLOSELOSS = { -5.0, -3.2, -2.0, -1.0, -0.4 };

	private Map<Team, Integer> tiers;

	public TieredRanking(Collection<Team> teams) {
		rawMap = new HashMap<>();
		tiers = new HashMap<>();
		for (Team team : teams) {
			tiers.put(team, 2); // Start every team off at the third tier
			rawMap.put(team, 0.0);
		}
		Map<Team, Double> nextGen = new HashMap<>();

		for (int i = 0; i < 129; i++) {
			for (Team team : teams) {
				double total = 0.0;
				for (Game game : team.getGames()) {
					total += getScore(team, game);
				}
				double average = total / team.getGames().size();
				nextGen.put(team, average);
			}
			// After each generation, reset tiers.
			// Start with a first tier of size 4
			int position = 4;
			int tierSize = 4;
			int counter = 0;
			int tier = 4;
			for (Team team : teams) {
				double next = nextGen.get(team);
				rawMap.put(team, next);
			}
			for (Team team : this.getRanking()) {
				counter++;
				if (counter > position && tier > 0) {
					tier--;
					tierSize = tierSize * 2;
					position += tierSize;
				}
				tiers.put(team, tier);
			}
		}
	}

	@VisibleForTesting
	public double getScore(Team team, Game game) {
		if (game.getWinner() == team) {
			int tier = tiers.get(game.getLoser());
			if (game.getMargin() > 8) {
				return CLEARWIN[tier];
			} else {
				return CLOSEWIN[tier];
			}
		} else {
			int tier = tiers.get(game.getWinner());
			if (game.getMargin() > 8) {
				return CLEARLOSS[tier];
			} else {
				return CLOSELOSS[tier];
			}
		}
	}

	@Override
	public String getFormulaName() {
		return "TierRank";
	}

}
