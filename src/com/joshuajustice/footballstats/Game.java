package com.joshuajustice.footballstats;

/**
 * Basic representation of a game.
 * 
 * @author Joshua Justice
 */
public class Game {
	private Team home;
	private int homeScore;
	private Team away;
	private int awayScore;

	public Game(Team away, int awayScore, Team home, int homeScore) {
		this.away = away;
		this.awayScore = awayScore;
		this.home = home;
		this.homeScore = homeScore;
		// Make sure that both teams have a reference to the game.
		away.addGame(this);
		home.addGame(this);
	}

	public Team getAwayTeam() {
		return away;
	}

	public Team getHomeTeam() {
		return home;
	}

	public int getScore(Team team) {
		if (team == home) {
			return homeScore;
		} else if (team == away) {
			return awayScore;
		} else {
			throw new IllegalArgumentException("That team didn't play in this game!");
		}
	}

	// No ties in CFB, so getWinner, getLoser, and getMargin all depend on that
	// fact.
	public Team getWinner() {
		if (homeScore > awayScore) {
			return home;
		} else {
			return away;
		}
	}

	public Team getLoser() {
		if (homeScore > awayScore) {
			return away;
		} else {
			return home;
		}
	}

	/**
	 * The margin is always regarded as positive.
	 */
	public int getMargin() {
		if (homeScore > awayScore) {
			return homeScore - awayScore;
		} else {
			return awayScore - homeScore;
		}
	}

	@Override
	public String toString() {
		return "" + getWinner() + " " + getScore(getWinner()) + " - " + getLoser() + " " + getScore(getLoser());
	}

}
