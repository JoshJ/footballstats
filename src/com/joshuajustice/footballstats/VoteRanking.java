package com.joshuajustice.footballstats;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import com.joshuajustice.footballstats.calculator.Calculator;

public class VoteRanking {

	Collection<Calculator> calculators;
	List<String> formulaNames;
	List<List<Team>> ballots;
	Map<Integer, Team> finalRankings;
	TotalRanking total;
	
	public VoteRanking(Collection<Calculator> calculators, TotalRanking total) {
		this.calculators = calculators;
		formulaNames = new ArrayList<>();
		this.total = total;
		finalRankings = new HashMap<>();
		ballots = new ArrayList<>();

		for (Calculator calculator : calculators) {
			ballots.add(calculator.getRanking());
			formulaNames.add(calculator.getFormulaName());
		}

		for (int i = 1; i < total.getFinalRankings().size() + 1 ; i++) {
		//for (int i = 1; i < 14 ; i++) {
			//System.out.println("\n===============GENERATION: " + i);
			Team next = vote();
			finalRankings.put(i, next);
			for (List<Team> ballot : ballots) {
				ballot.remove(next);
			}
		}
	}
	
	private Team vote() {
		List<List<Team>> thisRoundBallots = new ArrayList<>();
		for (List<Team> ballot : ballots) {
			List<Team> thisRoundBallot = new ArrayList<>();
			for (Team team : ballot) {
				thisRoundBallot.add(team);
			}
			thisRoundBallots.add(thisRoundBallot);
		}
		int numberOfTeams = ballotSize(thisRoundBallots);
		for (int i = 1; i < numberOfTeams + 1; i++) {
			//System.out.println("\n----------------------innerloop: " + i + "  ");
			Map<Team, Integer> thisRoundResults = votingRound(thisRoundBallots);
			//for(Map.Entry<Team, Integer> result : thisRoundResults.entrySet()) {
			//	System.out.println("" + result.getKey() + " " + result.getValue() + "  ");
			//}
			if (ballotSize(thisRoundBallots) == 1) {
				// we have one team left, that's the end of this round
				return thisRoundBallots.get(0).get(0);
			}			
			Team worst = getWorstTeam(thisRoundResults);
			for(List<Team> ballot : thisRoundBallots) {
				ballot.remove(worst);
			}
		}
		throw new IllegalArgumentException("We somehow don't return a team here.");
	}
	
	private Map<Team, Integer> votingRound(List<List<Team>> thisRoundBallots) {
		int numberOfTeams = ballotSize(thisRoundBallots);
		Map<Team, Integer> results = new HashMap<>();
		for (List<Team> ballot : thisRoundBallots) {
			for (int i = 0; i < numberOfTeams; i++) {
				Team team = ballot.get(i);
				if (results.get(team) == null) {
					results.put(team, numberOfTeams - i);
				} else {
					results.put(team, results.get(team) + numberOfTeams - i);
				}
			}
		}
		return results;
	}

	private Team getWorstTeam(Map<Team, Integer> thisRoundResults) {
		Team worst = null;
		int lowestScore = Integer.MAX_VALUE;
		for (Team team: thisRoundResults.keySet()) {
			if (thisRoundResults.get(team) < lowestScore) {
				worst = team;
				lowestScore = thisRoundResults.get(team);
			} else if (thisRoundResults.get(team) == lowestScore) {
				// If there's a tie, use the total average as the tiebreaker.
				double worstTotal = total.getFinalRankings().get(worst);
				double teamTotal = total.getFinalRankings().get(team);
				if (teamTotal > worstTotal) {
					worst = team;
				}
			}
		}
		return worst;
	}
	
	private int ballotSize(List<List<Team>> thisRoundBallots) {
		// sanity check
		Integer last = null;
		for (List<Team> ballot : thisRoundBallots) {
			if (last != null) {
				if (ballot.size() != last) {
					throw new IllegalArgumentException("The ballots were not the same size"); 
				}
			} else {
				last = ballot.size();
			}
		}
		if (last == null) {
			last = 0;
		}
		return last;
	}
	
	/**
	 * This probably should be a CSVParser-based thing but if someone puts a
	 * comma in a team name they can well and truly go fuck themselves.
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Rank,Team");
		builder.append("\n");
		for (int i = 1; i < finalRankings.size() + 1; i++) {
			Team team = finalRankings.get(i);
			builder.append(i + "," + team.toString() + "\n");
		}
		return builder.toString();
	}
	
	/**
	 * Like toString, but with a Markdown table
	 */
	public String toMarkdown() {
		StringBuilder builder = new StringBuilder();
		builder.append("Rank|Team|Average Rank");
		for (String name : formulaNames) {
			builder.append("|" + name);
		}
		builder.append("\n");
		builder.append(":--|:--|:--|");
		IntStream.range(0, 10).forEach(x -> {
			builder.append(":--|");
		});
		
		Map<Team, List<Integer>> rankings = total.getRankings();
		for (int i = 1; i < finalRankings.size() + 1; i++) {
			// We want to output the team name, the final ranking, then all the
			// original rankings
			Team team = finalRankings.get(i);
			builder.append("\n" + i + "|" + team.toString() + "|");
			double finalAverage = total.getAverageForTeam(team);
			builder.append(String.format("%4.5f", finalAverage));
			List<Integer> originals = rankings.get(team);
			for (Integer original : originals) {
				builder.append("|" + original);
			}
			
		}
		return builder.toString();

	}

}
