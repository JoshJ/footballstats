package com.joshuajustice.footballstats;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.IntStream;

import com.joshuajustice.footballstats.calculator.Calculator;
import com.joshuajustice.footballstats.calculator.TeamMapSorter;

public class TotalRanking {

	Collection<Calculator> calculators;
	List<String> formulaNames;
	Map<Team, List<Integer>> rankings;
	Map<Team, Double> finalRankings;

	/**
	 * Produce a total ranking from a collection of sub-rankings.
	 * 
	 * <p>
	 * The position in formulaNames matches the position in the values of
	 * rankings.
	 * 
	 * @param calculators
	 */
	public TotalRanking(Collection<Calculator> calculators) {
		this.calculators = calculators;
		formulaNames = new ArrayList<>();
		rankings = new HashMap<>();
		finalRankings = new HashMap<>();

		for (Calculator calculator : calculators) {
			formulaNames.add(calculator.getFormulaName());
			List<Team> teams = calculator.getRanking();
			int position = 0;
			for (Team team : teams) {
				position++;
				addRanking(team, position);
			}
		}

		for (Entry<Team, List<Integer>> entry : rankings.entrySet()) {
			List<Integer> list = entry.getValue();
			double sum = 0.0;
			for (Integer position : list) {
				sum += position;
			}
			double mean = sum / (double) list.size();
			finalRankings.put(entry.getKey(), mean);
		}
	}

	private void addRanking(Team team, int position) {
		if (rankings.get(team) == null) {
			List<Integer> list = new ArrayList<>();
			rankings.put(team, list);
		}
		rankings.get(team).add(position);
	}

	public Map<Team, Double> getFinalRankings() {
		return finalRankings;
	}
	
	public double getAverageForTeam(Team team) {
		return finalRankings.get(team);
	}
	
	public Map<Team, List<Integer>> getRankings() {
		return rankings;
	}
	
	/**
	 * This probably should be a CSVParser-based thing but if someone puts a
	 * comma in a team name they can well and truly go fuck themselves.
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		/*
		for (Calculator calculator : calculators) {
			builder.append(calculator.toString() + "\n");
		}
		*/
		builder.append("Rank,Team,Average Rank");
		for (String name : formulaNames) {
			builder.append("," + name);
		}

		List<Entry<Team, Double>> sortedFinal = TeamMapSorter.sortMapWithValues(finalRankings);
		int rank = 0;
		for (Entry<Team, Double> entry : sortedFinal) {
			rank++;
			builder.append("\n" + rank + ",");
			// We want to output the team name, the final ranking, then all the
			// original rankings
			double finalAverage = entry.getValue();
			builder.append(entry.getKey() + "," + String.format("%4.5f", finalAverage));
			List<Integer> originals = rankings.get(entry.getKey());
			for (Integer original : originals) {
				builder.append("," + original);
			}
		}

		return builder.toString();
	}
	/**
	 * Like toString, but with a Markdown table
	 */
	public String toMarkdown() {
		StringBuilder builder = new StringBuilder();
		builder.append("Rank|Team|Average Rank");
		for (String name : formulaNames) {
			builder.append("|" + name);
		}
		builder.append("\n");
		builder.append(":--|:--|:--|");
		IntStream.range(0, 10).forEach(x -> {
			builder.append(":--|");
		});
		
		List<Entry<Team, Double>> sortedFinal = TeamMapSorter.sortMapWithValues(finalRankings);
		int rank = 0;
		for (Entry<Team, Double> entry : sortedFinal) {
			rank++;
			builder.append("\n" + rank + "|");
			// We want to output the team name, the final ranking, then all the
			// original rankings
			double finalAverage = entry.getValue();
			builder.append(entry.getKey() + "|" + String.format("%4.5f", finalAverage));
			List<Integer> originals = rankings.get(entry.getKey());
			for (Integer original : originals) {
				builder.append("|" + original);
			}
		}

		return builder.toString();
	}
}
